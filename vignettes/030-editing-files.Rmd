---
title: "Editing files on the CHPC server"
date: "`r Sys.Date()`"
author: "Matthieu Bruneaux"
---

## Overview

Often, you will be preparing your scripts locally, on your own computer, and then transfer then to the server using `scp`. 

However, it is frequent to have bugs and typos present in the scripts that are only detected once you try to run them on the cluster. Being able to quickly edit the scripts directly on the server is more convenient for this kind of quick edits than modifying the local script and using `scp` again to transfer the updated file.

*(But remember to download a copy of the working script to your computer for your own archiving once you get it running on the CHPC server!)*

## The nano program

You can edit any text file directly on the server with the `nano` program. For example, to edit `my-script.R`, you would type:

```{bash eval = FALSE}
nano my-script.R
```

This will open the text editor in your terminal.

`nano` is quite basic but is more than enough for simple editing work. You can use the keyboard arrow keys to move around the text, and the bottom of the screen contains the description of keyboard shortcuts for common actions such as saving and exiting.

The keyboard shortcuts are executed by pressing CTRL and a letter simultaneously. For example, `^X Exit` means that you can exit the program by pressing CTRL + "x" at the same time.

### Main keyboard shortcuts

- CTRL + "x": exit nano (it will ask you if you want to save the file first if you have modified it)

- CTRL + "o": save the file

- CTRL + "g": nano's help

- CTRL + "k": delete the current line

- CTRL + "c": cancel a keyboard shortcut
