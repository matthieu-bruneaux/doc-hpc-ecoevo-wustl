[![GitLab pipeline status](https://gitlab.com/matthieu-bruneaux/doc-hpc-ecoevo-wustl/badges/master/pipeline.svg)](https://gitlab.com/matthieu-bruneaux/doc-hpc-ecoevo-wustl/commits/master)

# Documentation for HPC resources in the Eco-Evo team (WashU, St Louis)

This website contains **general information** and **tutorials** about the High Performance Computing (HPC) resources that can be used by the members of the Gordon and López-Sepulcre labs at Washington University.

The computing resources are made available to the Department of Biology by the [Center for High Performance Computing (CHPC)](https://biology.wustl.edu/high-performance-computing-resources).

## Quick start

- For new members, a [request for an account](https://www.mir.wustl.edu/research/research-support-facilities/center-for-high-performance-computing-chpc/services/request-an-account) must be made by the group leader.
- Once your account is created, you will need to call the CHPC contact person to get your account password.
- After that, you are ready to log in.

## Acknowledging the use of CHPC in your research

The following text should be used when [citing the CHPC resources in your research](http://mgt2.chpc.wustl.edu/wiki119/index.php/How_do_I_cite_the_CHPC_in_my_paper%3F):
 
*Computations were performed using the facilities of the Washington University Center for High Performance Computing, which were partially provided through NIH grant S10 OD018091.*

## Useful resources

- The wiki of the CHPC: [link](http://mgt2.chpc.wustl.edu/wiki119/index.php/Main_Page)

## Contact

If you have any questions, you can try to contact:

- for everyday-use questions: Matthieu Bruneaux (matthieu.d.bruneaux@jyu.fi)
- for more advanced, technical questions: Malcolm Tobias (mtobias@wustl.edu) and Xing Huang (x.huang@wustl.edu) who are working at the CHPC.
